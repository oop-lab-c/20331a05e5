class Student
{
    String collegeName, fullName;
    int collegeCode;
    double semPercentage;
    Student()
    {
        collegeName = "MVGR";
        collegeCode = 33;
    }
    Student(String name,double sempercent)
    {
        fullName = name;
        semPercentage = sempercent;
    }
    public static void main(String[] args)
    {
        Student obj1 = new Student();
        Student obj2 = new Student("sailu",92.81);
        System.out.println("college name : " + obj1.collegeName);
        System.out.println("college code : "+ obj1.collegeCode);
        System.out.println("full name : "+ obj2.fullName);
        System.out.println("sem percentage : "+ obj2.semPercentage);
    }
}
