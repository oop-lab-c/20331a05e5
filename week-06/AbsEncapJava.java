class AccessSpecifierDemo
{
    private int privar;
    protected int provar;
    public int pubvar;
    public void setvar(int privalue,int provalue,int pubvalue)
    {
        privar = privalue;
        provar = provalue;
        pubvar = pubvalue;
    }
     void getvar()
    {
        System.out.println("the privalue is "+privar);
        System.out.println("the provalue is "+provar);
        System.out.println("the pubvalue is "+pubvar);
    }
    public static void main(String[] args)
    {
        AccessSpecifierDemo b = new AccessSpecifierDemo();
        b.setvar(40,50,60);
        b.getvar();
    }
}