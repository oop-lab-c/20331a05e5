#include<iostream>
using namespace std;
class operations
{
    public:
    void add(){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"Sum : "<<a+b<<endl;
    }
    void sub()
    {
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"Subtraction : "<<a-b<<endl;
    }
};
//derived class
class multiplication:public operations{
    public:
    void multi()
    {
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"Multiplication : "<<a*b<<endl;
    }
};
int main()
{
    multiplication obj;
    obj.add();
    obj.multi();
    obj.sub();
    return 0;
}
