class PublicInheritance
{  
    int x=77;  
} 
class PrivateInheritance 
{
   private int numl;
   public void setpri(int n) 
  { 
     numl = n;
  } 
  public int getpri() 
  { 
     return numl;
  } 
} 
 class ProtectedInheritance 
{
  protected int num2;
  public void setprot(int n) 
  {
    num2 = n;
  } 
  public int getprot() 
  { 
    return num2;
  } 
} 
class DerivedPriv extends  PrivateInheritance
{ 
    public void mul() 
    {
        int num = getpri();
        System.out.println("product = " + (num *num)); 
    }
} 
class DerivedProt extends  ProtectedInheritance
{ 
    public void add() 
    {
        int num = getprot();
        System.out.println("sum = " + (num2 +num)); 
    }
}   
class DerivedPub extends PublicInheritance
{  
    int y = 60;  
}
class PPPInheriJava
{
  public static void main(String[] args)
  {
    DerivedPriv obj = new DerivedPriv();
    obj.setpri(10);
    obj.mul();
    DerivedProt a = new DerivedProt();
    a.setprot(10);
    a.add();
    DerivedPub b = new DerivedPub();
    System.out.println(b.x);
  }
}

