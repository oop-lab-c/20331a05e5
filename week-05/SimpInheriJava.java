class Animal
{
    void bark()
    {
        System.out.println("I can bark");
    }
}
class Dog extends Animal
{
    void walk()
    {
        System.out.println("I can walk");
    }

    public static void main(String[] args)
    {
        Dog d = new Dog();
        d.bark();
        d.walk();
    }
}