#include<iostream>
using namespace std;
class Parent
{
    private:
    int z = 1;
    int getpvt()
    {
        return z;
    }
    protected:
    int y = 2;
    public:
    int x = 3;
};
class publicinherit:public Parent{
    public:
    int getpub()
    {
        return x;
    }
    int getprot()
    {
        return y;
    }
};
class protectedinherit:protected Parent{
    public:
    int getpub()
    {
        return x;
    }
    int getprot()
    {
        return y;
    }
};
class privateinherit:private Parent{
    public:
    int getpub()
    {
        return x;
    }
    int getprot()
    {
        return y;
    }
};
int main()
{
    
    publicinherit obj;
    cout<<"public inheritance :\n";
    cout<<"public : "<<obj.getpub()<<endl;
    cout<<"private cant be accessed"<<endl;
    cout<<"protected : "<<obj.getprot()<<endl;
    protectedinherit objprot;
    cout<<"\n protected inheritance : \n";
    cout<<"public : "<<objprot.getpub()<<endl;
    cout<<"private cant be accessed"<<endl;
    cout<<"protected : "<<objprot.getprot()<<endl;
    privateinherit objpvt;
    cout<<"\n private inheritance : \n";
    cout<<"public : "<<objpvt.getpub()<<endl;
    cout<<"private cant be accessed"<<endl;
    cout<<"protected : "<<objpvt.getprot()<<endl;
    return 0;
}