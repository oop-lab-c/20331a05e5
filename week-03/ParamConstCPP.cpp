#include<iostream>
using namespace std;
class Student
{
    public:
        string collegeName, fullName;
        int collegeCode;
        double semPercentage;
        Student()
        {
            collegeName = "MVGR";
            collegeCode = 33;
        }
        Student(string name,double percent)
        {
            fullName = name;
            semPercentage = percent;
        }
};
int main()
{
    string name;
    double percent;
    Student obj1;
    Student obj2("sailu",92.81);
    cout<<"college name :"<<obj1.collegeName<<endl;
    cout<<"college code: "<<obj1.collegeCode<<endl;
    cout<<"full name : "<<obj2.fullName<<endl;
    cout<<"sem Percent "<<obj2.semPercentage<<endl;
    return 0;
}