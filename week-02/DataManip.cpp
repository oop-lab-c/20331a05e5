#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
int main()
{
    string name;
    float x = 34.4528;
    cout << setprecision(3) << x << endl;
    cout << setprecision(4) << x << endl;
    cout<<setw(10)<<"hello"<<endl; 
    cout<<"Enter Your name"<<endl;
    cin>>ws;//ws will ignore the blank spaces that are entered by the user before input value
    cin>>name;
    cout<<"Name is "<<name<<endl;
     cout <<setw(15) << setfill('-') << 35 << endl;
    cout<<"flush"<<flush;// flush will flush the next line same as ends
    cout<<"ends"<<ends;//ends is used to skip next line and continues to print the next outputs immediately
    cout<<"endl"<<endl;//endl is used to end the line on cout and print the next output from the next line..
}
