#include <iostream>
using namespace std;
void even(int n)
{
    if (n % 2 == 0) 
        cout << n<<" is even number"<<endl;
    else
        cout << n<<" is odd number"<<endl;
}
int main()
{
    int n;
    cout << "Enter a number :";
    cin >> n;
    even(n);
}
