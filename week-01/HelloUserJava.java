import java.util.Scanner;
class Hello
{
    void username() {
        System.out.println("Enter your name : ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        System.out.println("Hello " + name);
    }
    public static void main(String[] args)
    {
        Hello obj = new Hello();
        obj.username();
    }
}
